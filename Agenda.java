import java.util.*;
import java.io.*;
import java.nio.file.*;

public class Agenda {  
  
  static public void main(String[] args){
    Random rand = new Random();
    SequenzaOrdinata<Appuntamento> rubrica = new SequenzaOrdinata<Appuntamento>();
    java.io.Console console = System.console();
    int count = 0;
    int year;
    int month;
    int day;
    int hour;
    int min;
    String dsc;
    
    System.out.println("Quanti appuntamenti casuali vuoi creare?");
    int n = Integer.parseInt(console.readLine());
    
    /*
    while(count++ < n){
      year = rand.nextInt(5) + 2014;
      month = rand.nextInt(12) + 1;
      day = rand.nextInt(28) + 1;
      hour = rand.nextInt(8) + 9;
      min = rand.nextInt(60);
      dsc = "descriz. " + count;
      rubrica.add(new Appuntamento("" + day + "." + month + "." + year + " " + hour + ":" + min + " " + dsc));
    }
    */
    //System.out.println(salvaAgenda("/tmp/agenda.txt", rubrica));
    
    /*
    rubrica.add(new Appuntamento("04.03.2020 10:30 descrizione1"));
    rubrica.add(new Appuntamento("02.07.2011 11:30 descrizione2"));
    rubrica.add(new Appuntamento("02.03.2016 11:30 descrizione2dup"));
    rubrica.add(new Appuntamento("03.03.2015 13:30 descrizione3"));
    rubrica.add(new Appuntamento("02.03.2016 11:30 descrizione2dup"));
    rubrica.add(new Appuntamento("02.03.2012 11:30 descrizione2"));
    rubrica.add(new Appuntamento("02.03.2015 12:30 descrizione2"));
    */
        
    rubrica = caricaAgenda("/tmp/agenda.txt");
    for(Appuntamento a: rubrica){
      System.out.println(a);
    }

  }
  
  private static SequenzaOrdinata<Appuntamento> caricaAgenda(String p){
    try{
      SequenzaOrdinata<Appuntamento> agenda = new SequenzaOrdinata<Appuntamento>();
      Path path = Paths.get(p);
      List<String> loa = Files.readAllLines(path);
      for(String s: loa){
        agenda.add(new Appuntamento(s));
      }
      return agenda;
    }
    catch(Exception e){
      System.out.println(e);
      return null;
    }
    
  }
  
  private static boolean salvaAgenda(String p, SequenzaOrdinata<Appuntamento> rubrica){
    try {
      Path path = Paths.get(p);
      Files.deleteIfExists(path);
      Files.createFile(path);
      if( ! Files.isWritable(path) ) return false;
      for (Appuntamento a: rubrica){
        Files.write(path, ((a.getRaw())+"\n").getBytes(), StandardOpenOption.APPEND);
      }
      return true;
    }
    catch(Exception e){
      System.out.println(e);
      return false;
    }
    
  }
  
}


class Appuntamento implements Comparable<Appuntamento> {
  
  private String descr;
  private Orario ora;
  private GregorianCalendar data;
  
  public Appuntamento(String appunt){
    String[] items = appunt.split("\\s", 3);
    String[] dataItems = items[0].split("\\.");
    this.descr = items[2];
    this.ora = new Orario(items[1]);
    
    this.data = new GregorianCalendar(Integer.parseInt(dataItems[2]),
                                      Integer.parseInt(dataItems[1]),
                                      Integer.parseInt(dataItems[0]));
    //for(String s: items) System.out.println(s);
    //System.out.println(this.data);
    }
    
  
  public String toString(){
    return "Appuntamento in data " + this.data.get(GregorianCalendar.DAY_OF_MONTH) + "/" +
      this.data.get(GregorianCalendar.MONTH) + "/" + this.data.get(GregorianCalendar.YEAR) +
      " alle ore " + this.ora + "   motivo: " + this.descr;
  }
  
  public String getRaw(){
    //"02.03.2015 12:30 descrizione2"
    return "" + this.data.get(GregorianCalendar.DAY_OF_MONTH) + "." + this.data.get(GregorianCalendar.MONTH) + "." +
      this.data.get(GregorianCalendar.YEAR) + " " + this.ora + " " + this.descr;
  }
  
  public int compareTo(Appuntamento a){
    if(this.data.compareTo(a.data) ==  0) return this.ora.compareTo(a.ora);
    return this.data.compareTo(a.data);    
  }
}

