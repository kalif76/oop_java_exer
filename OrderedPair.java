import java.util.*; 

interface Pair<K, V>{
  public K getKey();
  public V getValue();
}

class Box<T>{
  private T e;

  public void setBox(T e){
    this.e = e;
  }

  public T getBox(){
    return this.e;
  }
}

public class OrderedPair<K, V> implements Pair<K, V>{
  private K key;
  private V value;

  public OrderedPair(K key, V value){
    this.key = key;
    this.value = value;
  }

  public K getKey(){
    return this.key;
  }

  public V getValue(){
    return this.value;
  }
}

