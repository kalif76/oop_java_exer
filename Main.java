public class Main {

    static public void main(String[] args){
        Frazione a = new Frazione(40,5);
        Frazione b = new Frazione(39,5);
        //System.out.println("Maggiore: " + a.isMaggiore(b));
        //System.out.println("Equal: " + a.equals(b));
        //System.out.println("Minore: " + a.isMinore(b));
        Orario o1 = new Orario();
        Orario o2 = new Orario("22:30");
        Orario o3 = new Orario(15059);
        Orario.setFormato24(false);
        System.out.println(o1.quantoManca(o2));
        System.out.println(o2);
        System.out.println(o1);
        System.out.println(o3);
        
    }
}
