import java.io.Console;

class Underline {
  /**
   * A class for printing underlined lines
   */

  private String line;

  public Underline(String txt){
    this.line = new String(txt);
  }

  public String toString(){
    return this.line.toLowerCase() + "\n" + this.underline();

  }

  public int getNLowerChars(){
    int count = 0;
    for(char c: this.line.toCharArray()){
      if(c >= 'a' && c <= 'z') count++;
    }
    return count;
  }

  private String underline(){
    String uline = "";

    int i = 0;
    while (i < this.line.length() ){
      uline = uline.concat("-");
      i++;
    }
    return uline;
  }

}

class Ese4_2 {
  static public void main(String[] args){
    Console c = System.console();
    System.out.println("Inserisci una frase: ");
    Underline line = new Underline(c.readLine());
    System.out.println(line);
    System.out.println("Lower case total: "+line.getNLowerChars());
  }
}


