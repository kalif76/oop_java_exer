public class Test {

    public static void main(String[] args){
      int x = 2;
      C alfa = new C();
      //alfa.assegna(x);
      //System.out.println(alfa instanceof B);
      //System.out.println(alfa);
      /*for(Dirs d: Dirs.values()){
        System.out.println(d);
      }*/
      
      System.out.println(Dirs.N.next());
      System.out.println(Dirs.W.next());
      System.out.println(Dirs.S.next());
      System.out.println(Dirs.E.next());

    }
}


enum Dirs {
  N, W, S, E;
  
  private N(
  
  public Dirs prev(){
    int idx = this.ordinal() == 0 ? Dirs.values().length -1 : this.ordinal() -1;
    return Dirs.values()[idx]; 
  }
  
    public Dirs next(){
    int idx = (this.ordinal() + 1) % this.values().length;
    return Dirs.values()[idx];
    
  }
}

class A {
  int k = 1;
  
  public A(){
    System.out.println("Building A");
  }
    public void assegna(int x){
        System.out.println("A::assegna::int");
    }
    public String toString(){
        return ""+this.k;
    }
}

class B extends A {
  int k = 2;
    public B(){
    System.out.println("Building B");
  }
    public void assegna(int x){
        System.out.println("B::assegna::int");
    }
    public void assegna(double x){
        System.out.println("B::assegna::double");
    }
    public String toString(){
      return "" + this.k + super.toString();
    }
}

class C extends B {
  int k = 3;
    public C(){
    System.out.println("Building C");
  }
    public void assegna(long x){
        System.out.println("C::assegna::long");
    }
    public void assegna(double x){
        System.out.println("C::assegna::double");
    }
    public String toString(){
       return "" + this.k + super.toString();
    }
    /*public void assegna(long x){
        System.out.println("C::assegna::long");
    }*/
}