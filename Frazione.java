import java.lang.Math.*;

public class Frazione implements Comparable<Frazione>{
    /** This class represent a rational number in the form of numerator/denominator
      */
         
    private int num;  // Numerator
    private int den;  // Denominator
    
    public Frazione(int n, int d){
        if(n >= 0 && d > 0 || n <= 0 && d < 0){
            this.num = n;
            this.den = d;
        } else {
            this.num = -(Math.abs(n));
            this.den = Math.abs(d);
        }
    }
    
    public Frazione(int n){
        this(n, 1);
    }
    
    public Frazione per(Frazione f){
         return (new Frazione(this.num * f.num, this.den * f.den)).lowestTerms();
    }
    
    public Frazione lowestTerms(){
        if(this.num == 0){
            return new Frazione(0,1);
        }
        
        int m = mcd();
        return new Frazione(this.num/m, this.den/m);
    }
    
    private int mcd(){
        int r;
        int a;
        int b;
        if(this.num == 0){
            System.out.println("Numerator = 0");
            return 1;
        }
        if(Math.abs(this.num) >= Math.abs(this.den)){
            a = Math.abs(this.num);
            b = Math.abs(this.den);
        } else {
            a = Math.abs(this.den);
            b = Math.abs(this.num);
        }
        
        r = a % b;
        
        while(r != 0){
            a = b;
            b = r;
            r = a % b;
        }
        
        return b;
    }
    
    public boolean equals(Frazione f){
        return this.meno(f).num == 0;
    }
    
    public boolean isMinore(Frazione f){
        Frazione diff = this.meno(f);
        if(diff.num >= 0) return false;
        return true;
    }
    
    public boolean isMaggiore(Frazione f){
        Frazione diff = this.meno(f);
        if(diff.num <= 0) return false;
        return true;
    }
    
    public Frazione piu(Frazione f){
        int newDen = this.den * f.den;
        int num1 = this.num * f.den;
        int num2 = f.num * this.den;
        return (new Frazione(num1 + num2, newDen)).lowestTerms();
    }
    
    public Frazione meno(Frazione f){
        return this.piu(new Frazione(-f.num, f.den));
    }
    
    public int compareTo(Frazione f){
      if (this.equals(f)) return 0;
      if (this.isMinore(f)) return -1;
      return 1;
    }
    
    public String toString(){
        return this.num + "/" + this.den;
    }
}