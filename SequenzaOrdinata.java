import java.util.*; 

public class SequenzaOrdinata<T extends Comparable<T>> implements Iterable<T> {
   
    private ArrayList<T> allElem ;
    
    public SequenzaOrdinata(){
        this.allElem = new ArrayList<T>();
        
    }
    
    public boolean add(T t){
      int i = 0;
      Iterator it = this.allElem.iterator();
      
      if(t == null) return false;
      
      if(this.allElem.size() == 0){
        this.allElem.add(t);
        return true;
      }
      
      while(i < this.allElem.size()){
        if(t.compareTo(this.allElem.get(i)) <= 0){
          this.allElem.add(i, t);
          return true;
        }
        i++;
      }

      this.allElem.add(t);
      return true;
    }
    
    public Iterator<T> iterator(){
        return this.allElem.iterator();
    }
    
    private void sort(){
    }
 
}

