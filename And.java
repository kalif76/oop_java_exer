class And {
  private OrderedPair<Integer, Boolean> ports[];
  
  public And(int n){
    ports = new OrderedPair<Integer, Boolean>[n];
  }
}