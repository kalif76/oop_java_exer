import java.lang.Math.*;

abstract class Shape {

  abstract double getArea();

  abstract double getPerimeter();

  boolean areaBiggerThan(Shape s){
    return (this.getArea() > s.getArea());
  }

  boolean perimeterBiggerThan(Shape s){
    return (this.getPerimeter() > s.getPerimeter());
  }

  public boolean equals(Shape s){
    if(this.getClass()  != s.getClass() ){
      return false;
    } else {
      if( this.getArea() == s.getArea() && this.getPerimeter() == s.getPerimeter()){
              return true;
      }
      return false;
    }
  }

}

class Circle extends Shape {

  protected double radius;

  public Circle(double r){
    this.radius = r;
  }

  public double getPerimeter(){
    return 2 * Math.PI * this.radius;
  }

  public double getRadius(){
      return this.radius;
  }

  public double getArea(){
    return (this.getPerimeter() * this.radius) / 2;
  }

  public String toString(){
    return "Radius = " + this.radius;
  }
}

class Rectangle extends Shape {

  protected double w, h, perim, area;

  public Rectangle(double w, double h){
    this.w = w;
    this.h = h;
  }

  public double getHeight(){
    return this.h;
  }

  public double getWidth(){
    return this.w;
  }

  public double getArea(){
    return this.w * this.h;
  }

  public double getPerimeter(){
    return this.w*2 + this.h*2;
  }

  public String toString(){
    return "Width = "+ this.w + ", Heigth = " + this.h;
  }
}


class Square extends Rectangle {

  public Square(double x){
    super(x, x);
  }

  public double getSide(){
    return this.w;
  }

  public String toString(){
    return "Side = "+ this.w;
  }
}


