import java.io.*;
import java.lang.*;
import java.util.*;

class IntegerParser {

    private String line;
    private String[] tokens;
    private int[] numbers;


    public IntegerParser(String line){
        this.line = line;
        this.tokens = line.split(" ");
        numbers = genIntArray();
    }

    private int[] genIntArray(){
        int[] nums = new int[this.tokens.length];
        int index = 0;
        for(String t: this.tokens){
            nums[index++] = Integer.parseInt(t);
        }
        return nums;
    }

    public int[] intArray(){
        return this.numbers;
    }

    static public void swap(int[] loi, int i, int j){
        int temp;
        temp = loi[i];
        loi[i] = loi[j];
        loi[j] = temp;

    }

    static public int[] bubbleSort(int[] nums){
        for(int j = 0; j < nums.length -1 ; j++){
            for(int i = 0; i < nums.length - 1; i++){
                if(nums[i] > nums[i+1]) swap(nums, i, i+1);
            }
        }
        return nums;
    }

}

class BubbleSort {

  static public void main(String[] args){
    String line;
    Console c = System.console();

    line = c.readLine();
    IntegerParser ip = new IntegerParser(line);
    for(Integer i: ip.intArray()){
      System.out.print("["+i.intValue()+"]-");
    }
    System.out.println();
    for(Integer i: IntegerParser.bubbleSort(ip.intArray())){
      System.out.print("["+i.intValue()+"]-");
    }
    System.out.println();

  }
}
