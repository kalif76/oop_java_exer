import java.util.GregorianCalendar;

public class Orario implements Comparable<Orario>{
  
  private int ore;
  private int min;
  private int sec;
  private static char sep = ':';
  private static boolean formato24 = true;
  
  public Orario(){
    GregorianCalendar gc = new GregorianCalendar();
    this.ore = gc.get(GregorianCalendar.HOUR_OF_DAY);
    this.min = gc.get(GregorianCalendar.MINUTE);
    this.sec = gc.get(GregorianCalendar.SECOND);
  }
  
  public Orario(int hh, int mm, int ss){
    this.ore = hh;
    this.min = mm;
    this.sec = ss;
  }
  
  public Orario(int hh, int mm){
    this(hh, mm, 0);
  }
  
  public Orario(int s){
    //int hh = s / 3600;
    //int mm = (s % 3600) / 60;
    //int ss = (s % 3600) % 60;
    this(s / 3600, (s % 3600) / 60, (s % 3600) % 60);
  }
  
  public Orario(String s){
    String loe[] = s.split(""+Orario.sep);
    this.ore = Integer.parseInt(loe[0]);
    this.min = Integer.parseInt(loe[1]);
    if(loe.length == 3) this.sec = Integer.parseInt(loe[2]);
    else this.sec = 0;
  }
  
  public static boolean isFormato24Attivo(){
    return Orario.formato24;
  }
  
  public static void setFormato24(boolean b){
    Orario.formato24 = b;
  }
  
  public int getOre(){
    return this.ore;
  }
  
  public int getMin(){
    return this.min;
  }
  
  public boolean equals(Orario o){
    return (this.min == o.min && this.ore == o.ore && this.sec == o.sec);
  }
  
  public boolean isMinore(Orario o){
    return this.ore * 3600 + this.min * 60 + this.sec < o.ore * 3600 + o.min * 60 + o .sec;
  }
  
  public boolean isMaggiore(Orario o){
    return this.ore * 3600 + this.min * 60 + this.sec > o.ore * 3600 + o.min * 60 + o .sec;
  }
  
  public int quantoManca(Orario o){
    return (o.ore * 3600 + o.min * 60 + o.sec) - (this.ore * 3600 + this.min * 60 + this.sec);
  }
  
  public static void setSeparatore(char c){
    Orario.sep = c;
  }
  
  public static char getSeparatore(){
    return Orario.sep;
  }
  
  
  public String toString(){
    String padMin = "";
    String padSec = "";
    String suffix = "";
    int ore12 = 0;
    
    if( ! Orario.formato24 ){
      if (this.ore > 12){
        ore12 = this.ore % 12;
        suffix = " PM";
      } else suffix = " AM";
    }
    if(this.min < 10) padMin = "0";
    if(this.sec < 10) padSec = "0";
    
    return "" + (this.ore % (Orario.formato24 ? 24 : 12)) + Orario.sep + padMin + this.min 
      + Orario.sep + padSec + this.sec + suffix;
  }
  
  public int compareTo(Orario o){
    if(isMinore(o)) return -1;
    if(equals(o))   return 0;
    return 1;
  }
  
}