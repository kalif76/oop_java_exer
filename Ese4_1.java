import java.util.ArrayList;

class LineParser {
  /**
   * The class representing a sentence
   */
  private String line;
  private int[] stats = new int[2];
  private String[] tokens;

  public LineParser(String txt){
    line = txt;
    tokens = line.split("\\s|\\W");
    stats = countChars();
  }

  public int getNChars(){
    return this.stats[0];
  }
  
  public int getNOccur(){
    return this.stats[1];
  }

  public String getLongestWord(){
    String longestWord = "";
    for(String word: this.tokens){
      if(word.length() > longestWord.length()){
        longestWord = new String(word);
      }

    }
    return longestWord;
  }

  private int[] countChars(){
    int[] stats = {0, 0};
    for(char ch: this.line.toCharArray()){
      if(ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'z' 
          || ch >= '0' && ch <= '9') stats[0] += 1;
      if(ch == 'A' || ch == 'a') stats[1] += 1;
    }
    return stats;
 }

  public int compareTo(LineParser lp){
    if(lp.getNChars() > this.stats[0]) return 2;
    if(lp.getNChars() == this.stats[0]) return 0;
    return 1;
  }

  public String toString(){
    return this.line;
  }
}


class Ese4_1 {

  static public void main(String[] args){
    System.out.println("Inserisci la prima frase e premi INVIO:");
    LineParser line1 = new LineParser(System.console().readLine());
    System.out.println("Inserisci la seconda frase e premi INVIO:");
    LineParser line2 = new LineParser(System.console().readLine());

    System.out.println("Totale lettere: " + (line1.getNChars() + line2.getNChars()));

    String word = line1.getLongestWord().length() >= line2.getLongestWord().length() ?
      line1.getLongestWord() : line2.getLongestWord();
    System.out.println("Parola più lunga: " + word);

    System.out.println("Occorrenze della lettera 'A' nella frase più lunga: " +
        (line1.getNChars() >= line2.getNChars() ? line1.getNOccur() : line2.getNOccur()) );


  }


}
