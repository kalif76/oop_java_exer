public enum Directions {
  NORTH(-1, 'y') ,
  SOUTH(1, 'y'),
  WEST(-1, 'x'),
  EAST (1, 'x');

  private int dir;
  private char axis;

  private Directions(int dir, char axis){
    this.dir = dir;
    this.axis = axis;
  }

  public boolean equals(Directions d){
    return false;
  }
  
  public String toString(){
    switch(this){
      case NORTH: return "North ^";
      case SOUTH: return "South v";
      case WEST: return "West <";
      default: return "East > ";
    }
  }
}

