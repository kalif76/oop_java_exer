/*
 * Scrivere una applicazione che richieda in input una serie di figure geometriche tra quelle messe a
 * disposizione dalla classe utili. Si calcolino poi le somme delle aree e dei perimetri di tutte le figure
 * inserite. Successivamente si calcoli il perimetro medio delle figure inserite e si stampino le
 * caratteristiche della figura il cui perimetro si avvicina maggiormente al valore medio
 * medio. Si stampi poi
 * il numero di quadrati, rettangoli e cerchi che hanno perimetro inferiore al valore medio. Si faccia
 * poi la stessa cosa per l’area.
 */
import java.util.*;
import java.util.regex.Pattern;
import java.lang.Math;

class Geometry {

  static private ArrayList<Shape> allShapes;

  static public void main(String[] args){
    String shape;
    allShapes = new ArrayList<Shape>();
    boolean keepAsk = true;

    Scanner scan = new Scanner(System.in);
    while(keepAsk){
      System.out.print("Insert kind of shape (circle | rectangle | square) end to stop: ");
      shape = scan.next();
      switch(shape){
        case "circle": createCircle(scan);
                       break;
        case "rectangle": createRectangle(scan);
                          break;
        case "square": createSquare(scan);
                       break;
        case "end": keepAsk = false;
              break;
        default: System.out.println(shape + ": no such shape in database...!");
                 break;
      }
    }

    double totArea = 0;
    double totPerim = 0;
    double aveArea = 0;
    double avePerim = 0;
    double deltaArea = 0;
    double deltaPerim = 0;
    double diff = 0;
    int index = 0;

    for(Shape s: allShapes){
      totArea += s.getArea();
      totPerim += s.getPerimeter();
    }

    aveArea = totArea / allShapes.size();
    avePerim = totPerim / allShapes.size();
    deltaArea = Math.abs(aveArea - allShapes.get(0).getArea());
    deltaPerim = Math.abs(avePerim - allShapes.get(0).getPerimeter());

    System.out.println("| = = = = = = = = = = = = = = = = |");
    System.out.println("|  total Area:      "+totArea);
    System.out.println("|  total Perimeter: "+totPerim);
    System.out.println("| = = = = = = = = = = = = = = = = |");

    System.out.println("| = = = = = = = = = = = = = = = = |");
    System.out.println("|  average Area:      "+aveArea);
    System.out.println("|  average Perimeter: "+avePerim);
    System.out.println("| = = = = = = = = = = = = = = = = |");

    for(Shape s: allShapes){
      diff = Math.abs(avePerim - s.getPerimeter());
      if(diff < deltaPerim){
        deltaPerim = diff;
        index++;
      }
    }

    System.out.println("Shape with the closest perimeter to the average: " + allShapes.get(index));

  }

  static private void createCircle(Scanner scan){
    int radius;
    System.out.print("Enter the radius value: ");
    radius = scan.nextInt();
    Geometry.allShapes.add(new Circle(radius));
  }


  static private void createRectangle(Scanner scan){
    int w, h;
    System.out.print("Enter the WIDTH and the HEIGHT values: ");
    w = scan.nextInt();
    h = scan.nextInt();
    Geometry.allShapes.add(new Rectangle(w, h));
  }

  static private void createSquare(Scanner scan){
    int side;
    System.out.print("Enter the side value: ");
    side = scan.nextInt();
    Geometry.allShapes.add( new Square(side));
  }
}


